module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "indent": [2, 4],
        "comma-dangle": ["error", "never"]
    },
    "env": {
        "browser": true
    }
};