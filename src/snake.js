export const up = 'u';
export const down = 'd';
export const left = 'l';
export const right = 'r';

// create snake
export const createSnake = positions => ({
    positions,
    direction: null,
    removedTail: null
});

// move forward
export const moveForward = (snake) => {
    let newPosition = null;
    // get last position
    const lastPosition = snake.positions[snake.positions.length - 1];

    // add new direction
    switch (snake.direction) {
    case up:
        newPosition = { x: lastPosition.x, y: lastPosition.y - 1 };
        break;
    case down:
        newPosition = { x: lastPosition.x, y: lastPosition.y + 1 };
        break;
    case left:
        newPosition = { x: lastPosition.x - 1, y: lastPosition.y };
        break;
    case right:
        newPosition = { x: lastPosition.x + 1, y: lastPosition.y };
        break;
    default:
        break;
    }

    if (newPosition) {
        const tail = snake.positions[0];
        // remove first item
        const newPositions = snake.positions.slice(1);

        // add new postions to front of snake
        newPositions.push(newPosition);

        return {
            positions: newPositions,
            removedTail: tail,
            direction: snake.direction
        };
    }
    return snake;
};

// change snake direction
export const changeDirection = (snake, direction) => {
    let newPosition = null;

    const currentDirection = snake.direction;
    // get last position
    const lastPosition = snake.positions[snake.positions.length - 1];

    // add new direction
    switch (direction) {
    case up:
        if (
            !currentDirection ||
                currentDirection === left ||
                currentDirection === right
        ) {
            newPosition = { x: lastPosition.x, y: lastPosition.y - 1 };
        }
        break;
    case down:
        if (
            !currentDirection ||
                currentDirection === left ||
                currentDirection === right
        ) {
            newPosition = { x: lastPosition.x, y: lastPosition.y + 1 };
        }
        break;
    case left:
        if (
            !currentDirection ||
                currentDirection === up ||
                currentDirection === down
        ) {
            newPosition = { x: lastPosition.x - 1, y: lastPosition.y };
        }
        break;
    case right:
        if (
            !currentDirection ||
                currentDirection === up ||
                currentDirection === down
        ) {
            newPosition = { x: lastPosition.x + 1, y: lastPosition.y };
        }
        break;
    default:
        break;
    }

    if (newPosition) {
        const tail = snake.positions[0];
        // remove first item
        const newPositions = snake.positions.slice(1);

        // add new postions to front of snake
        newPositions.push(newPosition);

        return {
            positions: newPositions,
            removedTail: tail,
            direction
        };
    }
    return snake;
};
