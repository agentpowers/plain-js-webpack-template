import random from 'lodash/random';
import * as snake from './snake';
import { createBoard, addClass, clearClassNames } from './helpers';

const snakeClassName = 'snake';
const foodClassName = 'food';

// game class
export default class Game {
    constructor(container, size, positions, food) {
        const { table, board } = createBoard(size, document);

        this.size = size;
        this.board = board;
        this.snake = snake.createSnake(positions);
        this.food = food;
        this.ended = false;

        this.changeSnakeDirection = this.changeSnakeDirection.bind(this);
        this.moveSnakeForward = this.moveSnakeForward.bind(this);

        // attach table to container
        container.appendChild(table);

        // apply initial snake
        this.applyNextMove();

        // apply food to board
        this.applyFoodToBoard();
    }

    moveSnakeForward() {
        // only if snake has direction set(has started)
        if (this.snake.direction) {
            // get next snake
            this.snake = snake.moveForward(this.snake);
            // apply snake to board
            this.applyNextMove();
        }
    }

    changeSnakeDirection(direction) {
        // get new snake
        this.snake = snake.changeDirection(this.snake, direction);
        // apply snake to board
        this.applyNextMove();
    }

    // apply food to board - will update dom
    applyFoodToBoard() {
        // process
        const cell = this.board[this.food.y][this.food.x];

        addClass(cell.ref, foodClassName);
    }

    // add new food and apply that to board
    addNewFood() {
        // create new food
        this.food = {
            x: random(this.size - 1),
            y: random(this.size - 1)
        };

        // apply food
        this.applyFoodToBoard();
    }

    // apply snake to board - will update dom
    applyNextMove() {
        const snakeHead = this.snake.positions[this.snake.positions.length - 1];

        // check to see if snake is going to eat food
        if (this.food.x === snakeHead.x && this.food.y === snakeHead.y) {
            // add removed tail back -- grow tail after eating food
            this.snake.positions = [this.snake.removedTail, ...this.snake.positions];
            this.snake.removedTail = null;
            // add new food
            this.addNewFood();
        }

        // process snake positions
        for (let i = 0; i < this.snake.positions.length; i += 1) {
            const snakeCell = this.snake.positions[i];
            const cell = this.board[snakeCell.y][snakeCell.x];

            addClass(cell.ref, snakeClassName);
        }

        // remove tail from board
        if (this.snake.removedTail) {
            const tailCellToRemove =
                    this.board[this.snake.removedTail.y][this.snake.removedTail.x];

            // clear classes
            clearClassNames(tailCellToRemove.ref);
        }
    }
}
