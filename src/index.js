import Game from './game';
import { up, down, left, right } from './snake';

import './index.less';

const size = 20;

// get container div from dom
const container = document.getElementById('container');

const initialSnakePositions = [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 0, y: 2 }];

// create game
const game = new Game(container, size, initialSnakePositions, { x: 10, y: 10 });

// set up tick
const tick = (timer) => {
    window.setTimeout(() => {
        // change snake direction
        game.moveSnakeForward();
        // tick
        tick(timer);
    }, timer);
};

const onKeyDown = (event) => {
    let direction = null;

    switch (event.key) {
    case 'ArrowDown':
        direction = down;
        break;
    case 'ArrowUp':
        direction = up;
        break;
    case 'ArrowLeft':
        direction = left;
        break;
    case 'ArrowRight':
        direction = right;
        break;
    default:
        break;
    }
    if (direction) {
        // change snake direction
        game.changeSnakeDirection(direction);
    }
};

// apply arrow key bindings
document.addEventListener('keydown', onKeyDown);

const tickMilliSecond = 100;

// start tick
tick(tickMilliSecond);
