// add provided class name to node - will update dom
export const addClass = (node, className) => {
    node.classList.add(className);
};

// remove provided class name to node - will update dom
export const removeClass = (node, className) => {
    node.classList.remove(className);
};

// clear class name - will update dom
export const clearClassNames = (node) => {
    node.className = ''; // eslint-disable-line
};

// create board and table element - board is a 2 dimensional array with ref to td
export const createBoard = (size, document) => {
    const board = [];
    const table = document.createElement('table');

    for (let x = 0; x < size; x += 1) {
        const row = [];
        const tr = document.createElement('tr');

        for (let y = 0; y < size; y += 1) {
            const td = document.createElement('td');

            tr.appendChild(td);
            row.push({ ref: td });
        }

        board.push(row);
        table.appendChild(tr);
    }

    return { table, board };
};
