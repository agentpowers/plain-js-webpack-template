Snake game

To run game 
- in dist folder open index.html
- OR using *webpack dev server*
    - in the root folder run the following commands
        - npm install
        - npm run build
        - npm run start


Next Steps 
- issues to fix
    - currently new food that is generated could be within snake's body(positions).  Make sure that the newly generated food position excludes current snake body.
    - handle snake hitting walls
        - while creating a new snake head, end game if the head of the snake is out of bounds(validate that x and y are between 0 and size of board).
    - handle snake hitting itself
        - while creating a new snake head, end game if the head of the snake is the same as any of existing snake positions.

- styling
    - add head and tail styling to snake.
- other functionality
    - after game ends, ask user if they would like to restart the game.
    - add pause option.
    - snake's speed increases as its size increases.
    - add score.
